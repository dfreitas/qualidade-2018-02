#include<stdio.h>
#include<locale.h>
#include "gtest/gtest.h"


int verificaIdentifier(const char *str);
int contaChar(const char *ponteiro);

TEST(CasosDeTeste, CT1)
{
  ASSERT_EQ(verificaIdentifier("abc12"), 1);
}

TEST(CasosDeTeste, CT2)
{
  ASSERT_EQ(verificaIdentifier("cont*1"), 0);
}

TEST(CasosDeTeste, CT3)
{
  ASSERT_EQ(verificaIdentifier("1soma"), 0);
}

TEST(CasosDeTeste, CT4)
{
  ASSERT_EQ(verificaIdentifier("a123456"), 0);
}

TEST(CasosDeTeste, CT5)
{
  ASSERT_EQ(verificaIdentifier("a1"), 1);
}

TEST(CasosDeTeste, CT6)
{
  ASSERT_EQ(verificaIdentifier(""), 0);
}

TEST(CasosDeTeste, CT7)
{
  ASSERT_EQ(verificaIdentifier("a b"), 0);
}

TEST(CasosDeTeste, CT8)
{
  ASSERT_EQ(verificaIdentifier(" abc"), 0);
}

TEST(CasosDeTeste, CT9)
{
  ASSERT_EQ(verificaIdentifier("A1b2C3d"), 0);
}

TEST(CasosDeTeste, CT10)
{
  ASSERT_EQ(verificaIdentifier("2B3"), 0);
}

TEST(CasosDeTeste, CT11)
{
  ASSERT_EQ(verificaIdentifier("Z#12"), 0);
}

TEST(CasosDeTeste, CT12)
{
  ASSERT_EQ(verificaIdentifier("\n"), 0);
}

TEST(CasosDeTeste, CT13)
{
  ASSERT_EQ(contaChar(""), 0);
}

TEST(CasosDeTeste, CT14)
{
  ASSERT_EQ(contaChar(" "), 1);
}

TEST(CasosDeTeste, CT15)
{
  ASSERT_EQ(verificaIdentifier("a"), 1);
}

TEST(CasosDeTeste, CT16)
{
  ASSERT_EQ(verificaIdentifier("1"), 0);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
