#include<stdio.h>
#include <ctype.h>
int contaChar(const char *ponteiro){ 
  int i = 0;

  for(;ponteiro[i] != 0; ++i);

  return i;
}

int verifica(const char *ponteiro, int tam){
  int cont = 0;

  if ( tam < 1 || tam > 6)
    return 0;

  if (isalpha(ponteiro[0]) == 0)
    return 0;
  
  while(cont < tam )
  {
    if (isalnum(ponteiro[cont]) == 0)
      return 0;
    cont++;
  }
  return 1;
}

int verificaIdentifier(const char *str)
{
  int tamanho;
  int resultado;
  tamanho = contaChar((char*)str);
  return verifica((char*)str, tamanho);
}